import {BtnFeatured, ViewProfile} from '@components/Elements'

const PropertyForm = () => {
    return (
        <div className="p-6 bg-white shadow-xl rounded-2xl">
            <ViewProfile
            avatar="http://placekitten.com/60/60"
            link="/"
            name="Laura Sanchez"
            />
            <form action="" className="flex flex-col space-y-3">
                <input type="text" placeholder="name" />
                <input type="text" placeholder="Phone" />
                <input type="email" placeholder="Email" />
                <textarea name="" placeholder="Hello, I am interested in…"></textarea>
                <button type="submit">
                    <BtnFeatured
                    className="justify-center w-full m-auto text-center transition-all shadow-md hover:shadow-xl btn--secundary">
                        Learn More
                    </BtnFeatured>
                </button>
            </form>
        </div>
    )
}

export default PropertyForm
