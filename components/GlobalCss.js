import tailwindConfig from "tailwind.config"
import 'tailwindcss/tailwind.css'

const GlobalCss = () => {
    console.log(tailwindConfig.theme);
  return (
    <>
    <style jsx global>{`
        * {
            font-family: ${tailwindConfig.theme.fontFamily.sans}
        }
        a {
            color: ${tailwindConfig.theme.extend.colors.primary.DEFAULT}
        }
        a:hover {
            color: ${tailwindConfig.theme.extend.colors.secundary}
        }
    `}</style>
    </>
  )
}

export default GlobalCss
