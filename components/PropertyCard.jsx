import PropertyDetails from "./PropertyDetails"
import Link from "next/link"

const PropertyCard = ({img, name, details}) => {
    return (
        <Link href={`/properties/${name}`}>
            <div className="overflow-hidden bg-white shadow-lg cursor-pointer rounded-xl hover:shadow-sm">
                <img src={img} alt={name} className="object-cover w-full h-64" />
                <h3 className="text-2xl font-bold p-7">{name}</h3>
                <PropertyDetails
                    list={details}
                />
            </div>
        </Link>
    )
}

export default PropertyCard
