import Link from 'next/link';
import Image from 'next/image'
import Logo from 'images/logo.svg'
import { BtnFeatured } from './Elements';

const Navbar = () => {
  return (
    <>
    <div className="container">
      <div className="grid items-center grid-cols-6 gap-4">
          <div className="col-span-2">
            <Link href="/">
                <a>
                    <Image
                    alt="Propiedades"
                    title="Propiedades"
                    src={Logo} />
                </a>
            </Link>
          </div>
          <div className="col-span-3">
              <nav>
                  <ul className="flex items-end justify-between text-white">
                      <li>Nav Link</li>
                      <li>Nav Link</li>
                      <li>Nav Link</li>
                      <li>Nav Link</li>
                  </ul>
              </nav>
          </div>
          <div className="col-span-1 text-right">
              <BtnFeatured className="btn--primary">Work with us</BtnFeatured>
          </div>
      </div>
    </div>
    </>
  )
}

export default Navbar;
