
import { MdKingBed, MdCalendarToday, MdBathtub, MdGridView, MdExpand } from 'react-icons/md'


const PropertyDetails = ({list}) => {
    function getIcon(name) {
        const info = {
            'bathroom': <MdBathtub/>,
            'bedrooms': <MdKingBed/>,
            'mts': <MdExpand/>,
            'year': <MdCalendarToday/>,
            'slot': <MdGridView/>,
          };
        return info[name];
    }

    const listItems = list.map((item) =>
        <li key={item.name} className="flex h-16 px-6 place-items-center">
            {getIcon(item.name)} <span className="ml-3">{item.text}</span>
        </li>
    );
    return (
        <>
            <ul className="flex justify-center bg-white border-t divide-x divide-gray border-gray">
                {listItems}
            </ul>
        </>
    )
}

export default PropertyDetails
