import {BigTitle} from '@components/Elements'
import PropertyCard from '@components/PropertyCard'

const SimilarListings = () => {
    return (
        <>
            <div className="flex justify-between">
                <BigTitle>Similar listings</BigTitle>
                <form action="">
                    <input type="text" placeholder="Search.." className="h-10 rounded-sm"/>
                    <select name="" id="">
                        <option value=""></option>
                    </select>
                </form>
            </div>

            <div className="grid grid-cols-3 gap-4 mt-10">
                <PropertyCard
                    name="Malto House"
                    img="http://placekitten.com/500/500"
                    details= {[
                        {name: 'bedrooms', text: '4'},
                        {name: 'bathroom', text: '2'},
                        {name: 'slot', text: '2'}
                    ]}
                />

<PropertyCard
                    name="Malto House"
                    img="http://placekitten.com/500/500"
                    details= {[
                        {name: 'bedrooms', text: '4'},
                        {name: 'bathroom', text: '2'},
                        {name: 'slot', text: '2'}
                    ]}
                />

<PropertyCard
                    name="Malto House"
                    img="http://placekitten.com/500/500"
                    details= {[
                        {name: 'bedrooms', text: '4'},
                        {name: 'bathroom', text: '2'},
                        {name: 'slot', text: '2'}
                    ]}
                />
            </div>
        </>
    )
}

export default SimilarListings
