import { BtnFeatured } from "./Elements"
import Image from 'next/image'
import Logo from 'images/logo.svg'
import Link from 'next/link';
import { FaFacebookF, FaTwitter, FaInstagram } from 'react-icons/fa'



const Footer = () => {
    return (
        <footer className="text-white bg-primary">
            <div className="container">
                <div className="flex items-center justify-between py-10 border-b border-white/10">
                    <h2 className="text-4xl font-bold text-white">Make your dreams a <span className="text-secundary">reality</span></h2>
                    <BtnFeatured className="py-5 btn--primary">Work with us</BtnFeatured>
                </div>
                <div className="flex flex-col justify-between py-10 md:flex-row">
                    <div className="mb-10 basis-1/6 md:mb-0">
                        <Link href="/">
                            <Image src={Logo} />
                        </Link>
                        <div className="flex justify-between mt-3 text-2xl">
                            <Link href="/"><FaFacebookF/></Link>
                            <Link href="/"><FaTwitter/></Link>
                            <Link href="/"><FaInstagram/></Link>
                        </div>
                    </div>
                    <div className="basis-4/6">
                        <div className="grid grid-cols-3 gap-4 text-white">
                            <div>
                                <h4 className="font-bold">Column heading</h4>
                                <ul>
                                    <li>Link goes here</li>
                                    <li>Link goes here</li>
                                    <li>Link goes here</li>
                                    <li>Link goes here</li>
                                </ul>
                            </div>
                            <div>
                                <h4 className="font-bold">Column heading</h4>
                                <ul>
                                    <li>Link goes here</li>
                                    <li>Link goes here</li>
                                    <li>Link goes here</li>
                                    <li>Link goes here</li>
                                </ul>
                            </div>
                            <div>
                                <h4 className="font-bold">Column heading</h4>
                                <ul>
                                    <li>Link goes here</li>
                                    <li>Link goes here</li>
                                    <li>Link goes here</li>
                                    <li>Link goes here</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer
