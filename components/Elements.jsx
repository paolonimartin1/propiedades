import tailwindConfig from "tailwind.config"
import {FaLongArrowAltRight} from 'react-icons/fa'
import Link from "next/link"

const BtnFeatured = ({icon=<FaLongArrowAltRight/>, className, children}) => {
    return (
      <span className={`${className ? className : ''}`}>
        {children}
        <span className="mx-2 text-xl">{icon}</span>
      </span>
    )
  }

const ViewProfile = ({avatar, name, link}) => {
  return (
    <>
    <div className="flex p-4 mb-4 bg-background rounded-xl">
        <img src={avatar} alt={name} className="object-cover w-10 h-10 mr-3 rounded-full" />
        <div>
          <h4 className="leading-tight">{name}</h4>
          <Link href={link}>
            <a className="font-bold secundary">View profile</a>
          </Link>
        </div>
    </div>
    </>
  )
}

const BigTitle = ({children}) => {
  return (
    <>
      <h2 className="inline-block text-5xl font-bold">{children}</h2>
      <style jsx>{`
      h2:before {
        content: "";
        display: block;
        height: 4px;
        width: 40%;
        border-radius: 2px;
        background: linear-gradient(90deg, ${tailwindConfig.theme.extend.colors.secundary} 0%, #000000 94.96%);
        margin-bottom: 1.4rem;
      }
      `}</style>
    </>
  )
}


export  {BtnFeatured, ViewProfile, BigTitle}
