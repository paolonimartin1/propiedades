import React from 'react'
import Navbar from '@components/Navbar'
import Footer from '@components/Footer'

const Layout = ({ children }) => {
  return (
    <>
      <header className="flex items-center bg-gradient-to-b from-black-2 to-black h-28">
        <Navbar/>
      </header>
      <main className="bg-background">
        {children}
      </main>
      <Footer/>
      <style jsx>{`
        header {
          border-bottom: 1px solid rgba(255,255,255, 0.1);
        }
      `}
      </style>
    </>
  )
}

export default Layout