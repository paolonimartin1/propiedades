export async function getStaticProps() {
    try {
      const res = await fetch("https://61aec09c2fd8100017ccd65c.mockapi.io/api/v1/rent")
      const data =await res.json()
      return {
        props: {
          data,
        }
      }
    } catch (error) {
      console.log(error);
    }
  }