import {useRouter} from 'next/router';
import PropertyForm from '@components/PropertyForm'
import PropertyDetails from '@components/PropertyDetails'
import SimilarListings from '@components/SimilarListings';

const property = ({data}) => {
    return (
        <>
        <div className="bg-primary text-primary-on">
            <div className="container flex items-center justify-between h-24">
                <h1 className="text-3xl font-bold">{data.name}</h1>
                <h2 className="text-3xl font-bold">${data.cost}</h2>
            </div>
        </div>
        <div className="container my-10">
            <div className="grid grid-cols-7 gap-6">
                <div className="col-span-7 lg:col-span-5">
                    <img src={data.src} alt={data.name} className='object-cover w-full cover-photo' height={520}/>

                    <div className="mt-6 overflow-hidden bg-white shadow-xl mb-9 rounded-3xl">
                        <h3 className="px-6 py-8 text-lg font-bold">Details</h3>
                        <PropertyDetails
                            list = {[
                            {name: 'bedrooms', text: data.bedrooms},
                            {name: 'bathroom', text: data.bathroom},
                            {name: 'year', text: data.year},
                            {name: 'slot', text: data.slot},
                            {name: 'mts', text: data.mts},
                            ]}
                        />
                    </div>
                </div>
                <div className="col-span-7 lg:col-span-2">
                    <PropertyForm></PropertyForm>
                </div>
            </div>
            <SimilarListings/>
        </div>
        <style jsx>{`
            .cover-photo {
                height: 520px;
                border-top-right-radius: 60px;
            }
        `}</style>
        </>
    )
}

export default property

export async function getStaticPaths() {
    try {
        const res = await fetch("https://61aec09c2fd8100017ccd65c.mockapi.io/api/v1/rent")
        const data =await res.json()
        const paths = data.map(({name}) => ({params: {name: `${name}`} }))
        console.log(paths)
        return {
            paths,
            fallback: false,
        }
    } catch (error) {
        console.log(error)
    }
}

export async function getStaticProps({params}) {
    try {
      const res = await fetch("https://61aec09c2fd8100017ccd65c.mockapi.io/api/v1/rent?name=" + params.name)
      const data = await res.json()
      return {
        props: {
          data: data[0]
        }
      }
    } catch (error) {
      console.log(error);
    }
  }
