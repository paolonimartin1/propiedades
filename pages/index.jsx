import PropertyCard from "@components/PropertyCard";

const Home = ({data}) => {

  return (
    <>
    <div className="container grid grid-cols-1 gap-6 py-20 xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2">
      {
        data.map((property) => (
          <PropertyCard
          key={property.name}
          img={property.src}
          name={property.name}
          details = {[
            {name: 'bedrooms', text: property.bedrooms},
            {name: 'bathroom', text: property.bathroom},
            {name: 'slot', text: property.slot}
          ]}
          />
        ))
      }

    </div>
    </>
  );

};

export async function getStaticProps() {
  try {
    const res = await fetch("https://61aec09c2fd8100017ccd65c.mockapi.io/api/v1/rent")
    const data =await res.json()
    return {
      props: {
        data,
      }
    }
  } catch (error) {
    console.log(error);
  }
}

export default Home;
