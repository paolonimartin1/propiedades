import Document, { Html, Head, Main, NextScript } from 'next/document'
import GlobalCss from '@components/GlobalCss'

class MyDocument extends Document {

  render() {
    return (
      <Html>
        <Head>
            <link rel="preconnect" href="https://fonts.googleapis.com" />
            <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
            <link href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@400;700&display=swap" rel="stylesheet" />
        </Head>
        <body className="text-primary">
          <GlobalCss/>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument