module.exports = {
  content: [],
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: {
          DEFAULT: '#000',
          on: '#fff'
        },
        secundary: '#FFAC12',
        background: '#F2F2F2',
        gray: {
          DEFAULT: '#CBD5E0',
          2: '#E2E8F0'
        },
        black: {
          DEFAULT: '#000',
          2: '#271A00'
        }
      },
    },
    fontFamily: {
      sans: ['DM Sans', 'sans-serif'],
    },
    container: {
      center: true,
    },
  },
  variants: {
    extend: {
        opacity: ['disabled'],
        backgroundColor: ['disabled'],
        textOpacity: ['disabled'],
    },
  },
  plugins: [],
}
